/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package student.reg;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author senzo
 */
public class Queue {

    List<Student> queue = new ArrayList();

    public void Enqueue(Student student) {
        queue.add(student);
    }

    public Student Dequeue() throws Exception {

        if (queue.isEmpty()) {
            throw new Exception("Your queue is empty!!!");
        }

        Student student = queue.get(0);
        queue.remove(0);

        return student;
    }

    public int Size(){
        return queue.size();
    }
}
