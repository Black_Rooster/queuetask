/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package student.reg;

/**
 *
 * @author senzo
 */
public class Student {

    private String Name;
    private String Surname;
    private String Gender;
    private String Id;
    private String CellphoneNumber;
    
    //Additional field{optional}
    private int Age;

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getCellphoneNumber() {
        return CellphoneNumber;
    }

    public void setCellphoneNumber(String CellphoneNumber) {
        this.CellphoneNumber = CellphoneNumber;
    }

    @Override
    public String toString() {
        return "Student{" + "Name=" + Name + ", Surname=" + Surname + ", Gender=" + Gender + ", Id=" + Id + ", CellphoneNumber=" + CellphoneNumber + '}';
    }

}
