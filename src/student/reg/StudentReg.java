/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package student.reg;

import java.util.Scanner;

/**
 *
 * @author senzo
 */
public class StudentReg {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Queue studentQueue = new Queue();
        StudentRegistration(studentQueue);
        NsfasApplication(studentQueue);
    }

    private static void StudentRegistration(Queue studentQueue) {

        Student student;
        Scanner input = new Scanner(System.in);
        String status;
        String temp;

        do {
            student = new Student();

            System.out.println("Please enter name");
            student.setName(input.nextLine());

            System.out.println("Please enter last name");
            student.setSurname(input.nextLine());

            System.out.println("Please enter gender");
            student.setGender(input.nextLine());

            System.out.println("Please enter identity number");
            student.setId(input.nextLine());

            System.out.println("Please enter cellphone number");
            student.setCellphoneNumber(input.nextLine());

            temp = student.getId().charAt(0) + "" + student.getId().charAt(1);
            student.setAge(CalculateAge(temp));

            if (student.getAge() != 0) {
                studentQueue.Enqueue(student);
                System.out.println("Number of registered student is :" + studentQueue.Size());
                System.out.println(student.toString());
            }

            System.out.println("Type done to close the registration on enter any content to continue");
            status = input.nextLine();

        } while (!status.equalsIgnoreCase("done"));
    }

    public static void NsfasApplication(Queue studentQueue) {

        try {
            String nsfasStatus;
            Student student;

            for (int i = 0; i <= studentQueue.Size(); i++) {
                student = studentQueue.Dequeue();
                
                if (student.getAge() > 16 && student.getAge() < 25) {
                    nsfasStatus = " NSFAS Status = Tuition and Accomodation.";
                } else if (student.getAge() > 25 && student.getAge() < 35) {
                    nsfasStatus = " NSFAS Status = Tuition only.";
                } else {
                    nsfasStatus = " NSFAS Status = Not granted.";
                }
                System.out.println(student.toString() + nsfasStatus);
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }

    private static int CalculateAge(String temp) {

        int intTemp = Integer.parseInt(temp);
        String year;
        int age = 0;

        if (intTemp >= 0 && intTemp <= 4) {
            year = "20" + temp;
            age = 2020 - Integer.parseInt(year);
        } else if (intTemp >= 20 && intTemp <= 99) {
            year = "19" + temp;
            age = 2020 - Integer.parseInt(year);
        }

        return age;
    }

}
